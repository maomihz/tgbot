# -*- coding: utf-8 -*-
import webapp2

from Telegram import *
from appenv import bot_tokens

from MainPage.Paste import Paste

class MaomiHzPasteBot(webapp2.RequestHandler):
    def post(self):
        update = Update(self.request.body)
        message = update.get_message()
        from_id = message.get_from().get_id()
        send = SendMessage(bot_tokens['maomihzpaste'])

        if not message:
            send.set_text(self.request.body)
            send.send()
            return
        text = message.get_text()
        send.set_chat_id(from_id)

        if text.startswith('/'):
            send.set_text('直接发信息给她，就会回复你链接了，支持Markdown哦～')
        else:
            if len(text) < 65535:
                msg_id = Paste.new_paste(text)['id']
                send.set_text('https://lk1.bid/' + msg_id)
            else:
                send.set_text('消息太长啦～猫猫小服务器表示不不不能忍！')

        send.set_disable_web_page_preview()
        print(send.send())
