# -*- coding: utf-8 -*-
import webapp2

from Telegram import *
from appenv import bot_tokens

class NFCompanionBot(webapp2.RequestHandler):
    def post(self):
        update = Update(self.request.body)
        send = SendMessage(bot_tokens['nfcompanion'])
        message = update.get_message()

        if not message:
            send.set_text(self.request.body)
            send.send()
            return

        from_id = message.get_from().get_id()
        text = message.get_text()

        send.set_chat_id(from_id)
        send.set_text('YOU KNOW WHAT, THIS BOT IS ALIVE!!!')

        send.send()
