��            )         �     �     �     �     �     �     �     �                    ,     ;  	   A  	   K     U     ^     l     |     �     �     �     �  
   �     �     �     �     �     �     �     �  {    $   �     �  !   �     �  	   �                    %     ,     ?  \  R     �     �     �     �     �               %     @     M     `     o     �     �     �  	   �     �     �                                                                                      
                           	                       CHANGE_PASSWORD CONTENT_NOT_FOUND CONTENT_TOO_LONG CONTENT_URL COPIED COPY COPY_ALL COPY_MANUALLY EDIT Generated Short URL INPUT_PASSWORD INTRO LANG_MANU MAIN_PAGE PASSWORD RECENT_PASTES REMOVE_PASSWORD SHORT_URL_GENERATED SUBMIT TITLE [EDIT] [HOME] [MARKDOWN] [MORE] [PASSWORD_PROTECTED] [PLAIN_TEXT] [PROTECTED] [RAW] [RESET_PASS] [VIEW_PASTE] Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-02-08 20:00-0600
PO-Revision-Date: 2017-02-08 20:01-0600
Last-Translator: 
Language-Team: 
Language: zh
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
X-Generator: Poedit 1.8.11
Plural-Forms: nplurals=1; plural=0;
 修改密码：（留空则不改） 对不起，内容无法找到 内容太长啦～缩短一些吧 内容／URL 已复制 复制 全部复制 手动复制 编辑 生成的短链接 请输入密码： ### 使用方法：

在下面框框里输入内容或链接，点击提交然后复制链接就行啦！

链接的话会直接跳转，Pastebin支持Markdown（这其实就不算什么Pastebin，模仿 [Telegra.ph](https://telegra.ph) 的一个玩意儿）

支持Telegram，和 [@MaomiHzPasteBot](https://t.me/maomihzpastebot) 对话就行啦！ <a href="?sl=en">[English]</a> 主页 密码（可选） 最近粘贴： 移除密码保护 短链接已生成 提交 猫猫短链接 & Pastebin 【编辑】 【返回首页】 【Markdown】 【显示更多】 【密码保护】 【无格式】 【密码保护的内容】 【Raw】 【重置输入的密码】 【查看链接】 