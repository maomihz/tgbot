��            )         �     �     �     �     �     �     �     �                    ,     ;  	   A  	   K     U     ^     l     |     �     �     �     �  
   �     �     �     �     �     �     �     �  n    +   z  !   �     �     �     �     �     �                    0  �   H  #   1  	   U     _     s     �     �     �     �  
   �     �     �     �     	          /  	   J     T     q                                                                                      
                           	                       CHANGE_PASSWORD CONTENT_NOT_FOUND CONTENT_TOO_LONG CONTENT_URL COPIED COPY COPY_ALL COPY_MANUALLY EDIT Generated Short URL INPUT_PASSWORD INTRO LANG_MANU MAIN_PAGE PASSWORD RECENT_PASTES REMOVE_PASSWORD SHORT_URL_GENERATED SUBMIT TITLE [EDIT] [HOME] [MARKDOWN] [MORE] [PASSWORD_PROTECTED] [PLAIN_TEXT] [PROTECTED] [RAW] [RESET_PASS] [VIEW_PASTE] Project-Id-Version: 
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2017-02-08 20:00-0600
PO-Revision-Date: 2017-02-08 20:01-0600
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
 Change Password: (Leave blank if unchanged) Sorry, content could not be found Sorry, content is too long~ Content / URL Copied Copy Copy All Copy Manually Edit Generated Short URL:  Please input password:  ### How to use：

Paste contents or URL into the input box above and hit submit. 

It behaves both like an URL shortener and Pastebin. 

There is a telegram bot for it! Just talk to [@MaomiHzPasteBot](https://t.me/maomihzpastebot). <a href="?sl=zh">[简体中文]</a> Main Page Password (Optional) Recent Pastes: Remove password protection Short URL Generated:  Submit Cat's URL Shortener & Pastebin 【Edit】 [Home] 【Markdown】 [Show more...] [Password Protected] 【Plain Text】 [Password Protected Paste] 【Raw】 [Reset Remembered Passwords] [View Link] 