# Translations template for PROJECT.
# Copyright (C) 2017 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2017.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2017-02-08 20:00-0600\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.3.4\n"

#: MainPage/html/base.html:1
msgid "TITLE"
msgstr ""

#: MainPage/html/base.html:7
msgid "[PROTECTED]"
msgstr ""

#: MainPage/html/base.html:12 MainPage/html/edit.html:5
msgid "EDIT"
msgstr ""

#: MainPage/html/edit.html:3 MainPage/html/submit.html:32
#: MainPage/html/view_message.html:9
msgid "[HOME]"
msgstr ""

#: MainPage/html/edit.html:5
msgid "[PASSWORD_PROTECTED]"
msgstr ""

#: MainPage/html/edit.html:8
msgid "CHANGE_PASSWORD"
msgstr ""

#: MainPage/html/edit.html:10
msgid "REMOVE_PASSWORD"
msgstr ""

#: MainPage/html/edit.html:15 MainPage/html/view_message.html:31
msgid "CONTENT_NOT_FOUND"
msgstr ""

#: MainPage/html/index.html:2
msgid "MAIN_PAGE"
msgstr ""

#: MainPage/html/index.html:6
msgid "LANG_MANU"
msgstr ""

#: MainPage/html/index.html:6
msgid "[RESET_PASS]"
msgstr ""

#: MainPage/html/index.html:12
msgid "CONTENT_URL"
msgstr ""

#: MainPage/html/index.html:14
msgid "PASSWORD"
msgstr ""

#: MainPage/html/index.html:16 MainPage/html/view_message.html:16
msgid "SUBMIT"
msgstr ""

#: MainPage/html/index.html:20
msgid "INTRO"
msgstr ""

#: MainPage/html/index.html:23
msgid "RECENT_PASTES"
msgstr ""

#: MainPage/html/index.html:25
msgid "[MORE]"
msgstr ""

#: MainPage/html/markdown.html:2
msgid "[PLAIN_TEXT]"
msgstr ""

#: MainPage/html/markdown.html:3 MainPage/html/plain.html:6
msgid "[RAW]"
msgstr ""

#: MainPage/html/plain.html:3
msgid "[EDIT]"
msgstr ""

#: MainPage/html/plain.html:5
msgid "[MARKDOWN]"
msgstr ""

#: MainPage/html/plain.html:16 MainPage/html/submit.html:13
msgid "COPIED"
msgstr ""

#: MainPage/html/plain.html:21 MainPage/html/submit.html:18
msgid "COPY_MANUALLY"
msgstr ""

#: MainPage/html/plain.html:25
msgid "COPY_ALL"
msgstr ""

#: MainPage/html/submit.html:2
msgid "Generated Short URL"
msgstr ""

#: MainPage/html/submit.html:22
msgid "SHORT_URL_GENERATED"
msgstr ""

#: MainPage/html/submit.html:24
msgid "COPY"
msgstr ""

#: MainPage/html/submit.html:27
msgid "CONTENT_TOO_LONG"
msgstr ""

#: MainPage/html/submit.html:34
msgid "[VIEW_PASTE]"
msgstr ""

#: MainPage/html/view_message.html:13
msgid "INPUT_PASSWORD"
msgstr ""

