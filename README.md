猫猫 短链接／Telegram Bot
=============

Beta 0.2.2

版本历史
-----

**Beta 0.2.4** 2017-02-08

- 修复了一些CSS和密码保护的问题
- 修复了数据库读取效率依然存在的问题

**Beta 0.2.3** 2017-02-08

- 支持了密码保护功能

**Beta 0.2.2** 2017-02-08

- 修复了查看内容HTML被解析的严重安全问题
- 支持编辑内容（有效期直到浏览器关闭）

**Beta 0.2.1** 2017-02-07

- 修复了没有指定协议类型导致跳转失败的问题

**Beta 0.2** 2017-02-07

- 数据库优化，读写量大大减少
- 删除了 “作者” 选项
- 修复了内容不能超过1500字节的问题

**Beta 0.1** 2017-01-18

新版上线！

猫猫短链不仅是一个链接缩短器，还是一个Pastebin，并且Pastebin里不仅可以存放纯文本，还可以有Markdown格式化的文本。

- 生成短链接
- 粘贴文本
- Markdown格式化文本
- Telegram Bot支持

简单说明
------
这是猫猫的一个短链接／Bot项目，整个项目是部署在Google App Engine Standard Environment上的，用的是Python2 / Jinja2。

涵盖的子项目：
- 猫猫短链接（https://lk1.bid）
- 猫猫Bot（[@MaomiHzBot](https://t.me/maomihzbot)）
- 短链接Bot（[@MaomiHzPasteBot](https://t.me/maomihzpastebot)）
- 奇怪的Bot（[@NFCompanionBot](https://t.me/nfcompanionbot)）

项目文件说明
------
1. GAE相关文件：
  * `app.yaml` app 相关配置
  * `appengine_config.py` 引擎相关配置
  * `index.yaml` 数据库索引配置
  * `main.py` 程序入口
  * `appenv.py` 一些全局变量（并非系统文件）
2. 第三方库文件：
  * `lib` 所有第三方库
  * `six.py` six库（放到lib里读不出不知道为什么）
3. 短链接相关文件
  * `MainPage/` 代码文件夹
  * |--> `handler/` 用户提交处理脚本
    * |--> `EditPageHandler.py` 用户编辑处理
    * |--> `PasteHandler` 短链接处理
  * |--> `page/` 页面显示脚本
    * |--> `EditPage.py` 编辑页面
    * |--> `IndexPage.py` 主页
    * |--> `PastePage.py` 详情页面
  * |--> `html/` Jinja2 模版
  * `locale/` 本地化文件（gettext格式）
  * `static/` 网页静态文件
  * `i18n.sh` 生成本地化文件的脚本（乱写的）
4. bot相关文件
  * `MaomiHzBot/` 猫猫 Bot
  * `MaomiHzPasteBot/` 短链接 Bot
  * `NFCompanionBot/` Mysterious Bot
  * `Telegram` 处理Telegram信息相关文件
5. 其他文件
  * `.DS_Store` 讨厌Mac的唯一理由
  * `.gitignore` Git 忽略配置

其他说明
-----
使用的库：
- bleach
- bleach_whitelist
- markdown
- html5lib
