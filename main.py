# -*- coding: utf-8 -*-
import webapp2

from MainPage import *
from MaomiHzBot import MaomiHzBot
from MaomiHzPasteBot import MaomiHzPasteBot
from NFCompanionBot import NFCompanionBot

from appenv import bot_tokens

config = {
    'webapp2_extras.sessions': {
        'secret_key': 'QvtXUtIcaiceUtmCE1wKAHFIz1e6WBfIdFbR1wQr',
        'session_max_age': 604800 # 7 days
    }
}

# Maomao URL Shortener Routes
url_shortener = webapp2.WSGIApplication([
    # The index page
    (r'/(?:index\.(?:html?|php))?', IndexPage),

    # Admin pages
    (r'/short\.do', PasteHandler),
    (r'/edit\.php', EditPage),
    ('/edit\.do', EditPageHandler),

    # Paste page
    (r'/([0-9a-z]+)/?([a-zA-Z]*)?', PastePage),

    # ACME Challenge Page
    webapp2.Route(r'/.well-known/acme-challenge/<challenge:.*>', handler=ACMEChallenge, name='challenge_str')
], debug=False, config = config)

# Telegram Bots Routes
bots = webapp2.WSGIApplication([
    ('/bot' + bot_tokens['maomihz'] + '/', MaomiHzBot),
    ('/bot' + bot_tokens['maomihzpaste'] + '/', MaomiHzPasteBot),
    ('/bot' + bot_tokens['nfcompanion'] + '/', NFCompanionBot)
], debug=True)
