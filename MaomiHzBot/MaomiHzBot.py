# -*- coding: utf-8 -*-
import webapp2
from google.appengine.api import memcache

import json
import codecs
import random
import string

import os
import base64

from Telegram import *

from appenv import bot_tokens, randstr_from

class MaomiHzBot(webapp2.RequestHandler):
    def post(self):
        token = bot_tokens['maomihz']
        memcache_prefix = 'tg_chat_to_'

        update = Update(self.request.body)
        message = update.get_message()

        send = SendMessage(token)
        msg = send.set_text

        if not message:
            msg(self.request.body)
            send.send()
            return

        from_id = message.get_from().get_id()
        send.set_chat_id(from_id)
        text = message.get_text()
        photo = message.get_photo()
        sticker = message.get_sticker()

        if not (text or photo or sticker):
            msg(self.request.body)
            send.send()
            return


        if text:
            command_obj = Command(text)
            command = command_obj.get_command()
            arguments = command_obj.get_arguments()
            # First, if it is a command
            if text.startswith('/'):

                # Fortune command
                if command == 'fortune':
                    fortune_file = open('MaomiHzBot/fortune.txt', 'r')

                    # Strip all empty lines from the fortune file
                    fortunes = filter(None, fortune_file.read().split('\n'))
                    msg(random.choice(fortunes))

                # random command
                elif command == 'random':

                    # default arguments
                    rand_format = 'hex'
                    rand_length = 16
                    arglen = len(arguments)

                    # Too many arguments
                    if arglen > 2:
                        rand_format = 'err'
                        msg('random takes at most 2 arguments. /help')
                    elif arglen > 0:
                        if arguments[0].isdigit():
                            rand_length = int(float(arguments[0]))
                            if arglen > 1:
                                rand_format = arguments[1]
                        else:
                            rand_format = arguments[0]
                            if arglen > 1:
                                rand_length = int(float(arguments[1]))

                    if rand_length < 0 or rand_length >= 512:
                        rand_format = 'err'
                        msg('Length can only be 0-512! /help')

                    if rand_format.lower() in ['hex']:
                        msg(randstr_from(rand_length, string.hexdigits.lower()))

                    elif rand_format.lower() in ['number', 'num', 'digit', 'digits']:
                        msg(randstr_from(rand_length, string.digits))

                    elif rand_format.lower() in ['alpha', 'alphabet', 'letter', 'letters']:
                        msg(randstr_from(rand_length, string.lowercase))

                    elif rand_format.lower() in ['alphanum']:
                        msg(randstr_from(rand_length, string.lowercase + string.digits))

                    elif rand_format.lower() in ['symbol', 'symbols', 'punc', 'punctuation']:
                        msg(randstr_from(rand_length, string.lowercase + string.uppercase + string.digits + string.punctuation))

                    elif rand_format.lower() in ['b64', 'base64']:
                        msg(base64.b64encode(os.urandom(rand_length)))

                    elif rand_format.lower() == 'err':
                        pass

                    else:
                        msg('Invalid format. /help')

                elif command == 'b64':
                    argument = string.split(text, ' ', 1)[1:]
                    if len(argument) == 0:
                        msg('Please specify the string to encode. /help')
                    else:
                        msg(base64.b64encode(argument[0].encode('utf-8')))

                elif command == 'setquery':
                    if arguments and arguments[0].isdigit():
                        # Set the query person
                        # If query already set:
                        original = memcache.get(memcache_prefix + str(from_id))
                        if original:
                            memcache.set(memcache_prefix + str(from_id), None)
                            memcache.set(memcache_prefix + str(original), None)

                        original2 = memcache.get(memcache_prefix + arguments[0])
                        if original2:
                            memcache.set(memcache_prefix + str(original2), None)

                        memcache.set(memcache_prefix + str(from_id), int(arguments[0]))
                        memcache.set(memcache_prefix + arguments[0], from_id)
                    else:
                        chat_to = memcache.get(memcache_prefix + str(from_id))
                        msg('You are chatting to ' + str(chat_to))

                elif command == 'whoami':
                    msg('Your ID = ' + str(from_id))

                elif command == 'help':
                    msg(open('MaomiHzBot/help.txt').read())
                elif command == 'start':
                    msg(open('MaomiHzBot/start.txt').read())
                    send.remove_reply_markup()

            else:
                chat_to = memcache.get(memcache_prefix + str(from_id))
                if chat_to:
                    send.set_chat_id(chat_to)
                    msg(command.encode('utf-8'))
                    if len(arguments) > 0:
                        markup = ReplyKeyboardMarkup()
                        markup.set_one_time_keyboard()
                        for i in arguments:
                            if i == '|':
                                markup.add_row()
                            else:
                                markup.add_key(i.encode('utf-8'))
                        send.set_reply_markup(markup)
                    else:
                        send.remove_reply_markup()
                else:
                    msg('no active chat. ')

            send.send()
        # End text message
        elif photo:
            chat_to = memcache.get(memcache_prefix + str(from_id))
            if chat_to:
                file_id = photo[-1].get_file_id()
                send = SendPhoto(token, file_id, chat_to)
                send.send()
            else:
                send = SendMessage(token)
                send.set_chat_id(from_id)
                send.set_text('No active chat. ')
                send.send()
        elif sticker:
            chat_to = memcache.get(memcache_prefix + str(from_id))
            if chat_to:
                sticker = sticker.get_file_id()
                send = SendSticker(token, sticker, chat_to)
                send.send()
            else:
                send = SendMessage(token)
                send.set_chat_id(from_id)
                send.set_text('No active chat. ')
                send.send()


        if from_id != 97669872:
            send = SendMessage(token)
            send.chat_id = '@maomaobotbroadcast'
            chat = message.get_chat()
            if text:
                send.set_text('[fwd]{} {}({}, @{}): {}'.format(
                    chat.get_first_name(),
                    chat.get_last_name(),
                    from_id,
                    chat.get_username(),
                    text.encode('utf-8')))
            send.send()
