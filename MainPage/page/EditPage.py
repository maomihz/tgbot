# -*- coding: utf-8 -*-
from ..BaseHandler import BaseHandler

from ..Paste import Paste
import appenv

class EditPage(BaseHandler):
    def get(self):
        # unicode --> str
        id = str(self.request.get('id'))
        # Get content data
        paste = Paste.get_by_id(id)
        if paste and paste['owner'] != self.session_id:
            self.response.write('Unauthorized')
            return

        # Write jinja template
        template_values = {
            'paste': paste,
        }
        template = appenv.JINJA_ENVIRONMENT.get_template('edit.html')

        # Write Headers
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(template.render(template_values))
