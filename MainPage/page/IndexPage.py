# -*- coding: utf-8 -*-
# Web lib
from ..BaseHandler import BaseHandler
import jinja2

# Custom modules
from ..Paste import Paste
import appenv

class IndexPage(BaseHandler):
    def get(self):
        # Handle setlang function
        setlang = self.request.get('sl')
        if setlang:
            self.response.set_cookie('lang', setlang, max_age=2147483647)
            self.redirect('/')
            return

        # Reset stored password function
        if self.request.get('resetpass'):
            self.session['auth_pass'] = {}
            self.redirect('/')
            return

        # Read the recent messages
        recent_pastes = Paste.get_latest(self.MAINPAGE_PASTE_COUNT)

        # Write jinja template
        template_values = {
            'recent_pastes': recent_pastes,
            'links': True,
        }
        template = appenv.JINJA_ENVIRONMENT.get_template('index.html')

        # Write content
        self.response.write(template.render(template_values))
