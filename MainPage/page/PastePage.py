# -*- coding: utf-8 -*-
from ..BaseHandler import BaseHandler
import jinja2

import re
import hashlib

from ..Paste import Paste
import appenv

class PastePage(BaseHandler):
    def get(self, id, display_format):
        URL_REGEX = re.compile('(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})')

        paste = None
        recent_pastes = None

        # ID=0 is a special page
        if id != '0': # Display the content of a message
            paste = Paste.get_by_id(id)

            auth = self.request.get('auth')
            print(self.session)
            if auth and len(auth) < 63:
                auth_pass = self.session.get('auth_pass') or {}
                auth_pass[id] = auth
                self.session['auth_pass'] = auth_pass
                self.redirect(self.request.path_url)
                return

            if not paste: # Invalid ID
                format = 'not found'
            elif paste['isProtected'] and paste['owner'] != self.session_id and not Paste.verify(id, self.session['auth_pass']):
                format = 'need auth'
            # If format == raw then return the content directly
            elif display_format in ['raw']:
                self.response.headers['Content-Type'] = 'text/plain'
                self.response.write(paste['content'])
                return

            # If the content is a link, then redirect
            elif not display_format and URL_REGEX.match(paste['content']) and  not re.compile('https?:\/\/lk1.bid.*').match(paste['content']) and not re.compile('https?:\/\/www.lk1.bid.*').match(paste['content']):
                url = paste['content']
                if not url.startswith('http'):
                    url = 'https://' + url
                self.redirect(url.encode('utf-8'))
                return

            # if format == plain text
            elif display_format in ['plain', 'code', '']:
                format = 'plain'

            # format == markdown
            elif display_format in ['md','markdown']:
                format = 'markdown'

            # Error request string, redirect back
            else:
                self.redirect('/' + Paste.id_to_str(id))
                return

        else:
            recent_pastes = Paste.get_latest(100)
            format = 'all'

        template_values = {
            'paste': paste,
            'format': format,
            'recent_pastes': recent_pastes,
        }
        template = appenv.JINJA_ENVIRONMENT.get_template('view_message.html')

        # Write response
        self.response.write(template.render(template_values))
