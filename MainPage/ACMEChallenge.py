import webapp2

class ACMEChallenge(webapp2.RequestHandler):
    def get(self, challenge):
        challenge_str = challenge
        challenge_dict = {
            'BlZOnHLTcMdRe6M_S9FI8n6gPa-A5d3XZvR3RILCpnQ':
            'BlZOnHLTcMdRe6M_S9FI8n6gPa-A5d3XZvR3RILCpnQ.XIv-WHYeeNdv2fQ24eg7J6N8hXi7SvQgNaf_Kq_h5Ys',
            '5M26c7JDbFBJxJs9LpARks7KJpc5yoAwOSUl0YSWJv4':
            '5M26c7JDbFBJxJs9LpARks7KJpc5yoAwOSUl0YSWJv4.XIv-WHYeeNdv2fQ24eg7J6N8hXi7SvQgNaf_Kq_h5Ys'
        }
        self.response.write(challenge_dict.get(challenge_str, 'Error'))
