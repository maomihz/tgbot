from google.appengine.ext import ndb
from google.appengine.api import memcache

import hashlib
from datetime import datetime

class Paste(ndb.Model):
    # Internally the paste object is represented by an object which the ID
    # is represented by a numeric number. Externally, a paste object is
    # represented by a dictionary with all properties and with ID represented
    # by a string.

    content = ndb.StringProperty(indexed=False)
    author = ndb.StringProperty(default='Anonymous')
    owner = ndb.StringProperty()

    id = ndb.IntegerProperty()

    date = ndb.DateTimeProperty(auto_now_add=True, indexed=False)
    content_sha1 = ndb.BlobProperty(indexed=True)

    isPrivate = ndb.BooleanProperty(default=False)
    isModified = ndb.BooleanProperty(default=False)
    isRemoved = ndb.BooleanProperty(default=False)

    isProtected = ndb.BooleanProperty(default=False)
    password_sha1 = ndb.BlobProperty(indexed=False)

    memcache_prefix = 'pastedb_'

    @classmethod
    def parent_key(cls):
        ''' get the parent key of the database '''
        return ndb.Key(cls.__name__, 'paste_db')

    @classmethod
    def get_last_id(cls):
        ''' Get the ID of the last paste, return id in internal form'''
        memcache_key = 'pastedb_last_id'

        # Load ID from memcache
        last_id = memcache.get(memcache_key)

        # Last id not in memcache
        if not last_id:
            # Get last id from the latest pastes
            last_msg = cls.query(ancestor=cls.parent_key()).order(-Paste.id).fetch(1)
            last_id = last_msg[0].id if last_msg else 0

            # Store back to memcache again
            memcache.set(memcache_key, last_id)

        return last_id


    @classmethod
    def new_paste(cls, content, author='Anonymous', owner=None, password=None):
        ''' Create a new paste, return paste represented in a dictionary '''

        # Check for already existed paste: author and content matches
        existing_msgs = cls.query(
            cls.content_sha1 == hashlib.sha1(content.encode('utf-8')).digest(), ancestor=cls.parent_key()
            ).filter(cls.author == author).fetch(1)

        if existing_msgs and not password:
            paste = existing_msgs[0]
        else:
            # Retrieve the most recent id
            last_id = cls.get_last_id()

            # Generate a new message
            paste = Paste(parent=cls.parent_key(),
                            content = content,
                            content_sha1 = hashlib.sha1(content.encode('utf-8')).digest(),
                            author = author,
                            id = last_id + 1,
                            owner = owner,
                        )
            if password:
                paste.password_sha1 = hashlib.sha1(password.encode('utf-8')).digest()
                isProtected = True
            paste.put()

            # Update memcache paste ID
            memcache.set('pastedb_last_id', last_id + 1)

        # Convert the newly created paste to dictionary
        paste_dict = cls.conv_msg_dict(paste)

        # Write the paste to memcache
        memcache.set(cls.memcache_prefix + cls.conv_id_str(paste.id), paste_dict)
        return paste_dict

    @classmethod
    def edit_paste(cls, id, content=None, password=None, rmpass=False):
        num_id = cls.conv_id_str(id)
        paste = cls.query(cls.id == num_id, ancestor=cls.parent_key()).fetch(1)

        # If there is no paste with specified ID exist
        if not paste:
            return None

        # Else choose the first element in the array
        paste = paste[0]
        if rmpass:
            paste.isProtected = False
            password = None

        if password or content:
            # Reset password of the paste
            if password:
                paste.password_sha1 = hashlib.sha1(password.encode('utf-8')).digest()
                paste.isProtected = True

            # Set the content and update necessary infomations
            if content:
                paste.content = content
                paste.date = datetime.now()
                paste.isModified = True
                paste.content_sha1 = hashlib.sha1(content.encode('utf-8')).digest()

            paste.put()

            # Update memcache also
            memcache_key = cls.memcache_prefix + id
            memcache.set(memcache_key, cls.conv_msg_dict(paste))

        return cls.conv_msg_dict(paste)

    @classmethod
    def verify(cls, id, grant_dict):
        # Verify password in grant dictionary
        paste = cls.get_by_id(id)
        if not paste:
            return False

        granted = False
        if id in grant_dict:
            pwd_sha1 = hashlib.sha1(grant_dict[id].encode('utf-8')).digest()
            granted = True if paste['password_sha1'] == pwd_sha1 else False
        return granted


    @classmethod
    def get_latest(cls, length):
        ''' Get latest pastes in descending order from largest ID '''
        query = cls.query(ancestor=cls.parent_key()).order(-Paste.id)
        query_result = query.fetch(length, keys_only=True)

        result = []
        last_id = cls.get_last_id()
        count = 0
        while length > 0 and last_id - count > 0:
            memcache_key = cls.memcache_prefix + cls.conv_id_str(last_id - count)
            cache = memcache.get(memcache_key)
            if not cache:
                cache = cls.conv_msg_dict(query_result[count].get())
                memcache.set(memcache_key, cache)
            result.append(cache)
            count = count + 1
            length = length - 1
        return result


    @classmethod
    def get_by_id(cls, id):
        # Convert ID string to internal ID
        if id.__class__ is str:
            id = cls.conv_id_str(id)

        memcache_key = cls.memcache_prefix + cls.conv_id_str(id)
        cache = memcache.get(memcache_key)
        if not cache:
            query_result = cls.query(Paste.id == id, ancestor=cls.parent_key()).fetch(1)
            if not query_result:
                return None
            cache = cls.conv_msg_dict(query_result[0])
            memcache.set(memcache_key, cache)

        return cache


    @classmethod
    def conv_msg_dict(cls, paste):
        # Internal message object to dictionary
        if paste.__class__ is Paste:
            result = {}
            result['content'] = paste.content
            result['author'] = paste.author
            result['owner'] = paste.owner
            result['id'] = cls.conv_id_str(paste.id)
            result['date'] = paste.date
            result['isModified'] = paste.isModified
            result['isPrivate'] = paste.isPrivate
            result['isRemoved'] = paste.isRemoved
            result['isProtected'] = paste.isProtected
            result['password_sha1'] = paste.password_sha1
            return result

    @classmethod
    def conv_id_str(cls, id):
        charset = '0123456789abcdefghijklmnopqrstuvwxyz'
        charset_len = len(charset)
        # String to internal ID
        if id.__class__ is str:
            result = 0
            for i, char in enumerate(reversed(id)):
                result = result + charset.index(char) * charset_len ** i
            return result
        # Internal ID to string
        elif id.__class__ is int:
            result = []
            while id > 0:
                result.append(charset[id % charset_len])
                id = id // charset_len
            return ''.join(reversed(result))
