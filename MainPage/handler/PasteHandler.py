# -*- coding: utf-8 -*-
from ..BaseHandler import BaseHandler

from ..Paste import Paste
import appenv

class PasteHandler(BaseHandler):
    def get(self):
        # Unsupported method GET
        self.response.write('Biong!!! 这能忍啊，回家家去！');

    def post(self):
        content = self.request.get('content')
        password = self.request.get('password')
        owner = self.session_id

        paste_data = None
        error_type = None

        if not content:
            error_type = 'no_content'
        elif len(content) > self.MAX_CONTENT_LENGTH or len(password) > 63: # Paste too long
            error_type = 'too_long'
        else:
            paste_data = Paste.new_paste(content = content, owner = owner, password = password)

        # Write jinja template
        template_values = {
            'paste': paste_data,
            'error_type': error_type,
        }
        template = appenv.JINJA_ENVIRONMENT.get_template('submit.html')

        # Write Response
        self.response.write(template.render(template_values))
