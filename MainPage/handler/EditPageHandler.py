from ..BaseHandler import BaseHandler
from ..Paste import Paste

import appenv

class EditPageHandler(BaseHandler):
    def post(self):
        id = str(self.request.get('id'))

        # Edit the content of the paste
        paste_content = self.request.get('content')
        password = self.request.get('password')
        rmpass = True if self.request.get('rmpass') else False
        paste = Paste.get_by_id(id)
        if paste['owner'] == self.session_id and len(paste_content) < self.MAX_CONTENT_LENGTH and len(password) < 63:
            Paste.edit_paste(id, content = paste_content, password = password, rmpass = rmpass)
            self.redirect('/' + id + '/plain')
        else:
            self.response.write('Unauthorized Action')
