import webapp2
import uuid

from webapp2_extras import sessions

import appenv
from Paste import Paste

class BaseHandler(webapp2.RequestHandler):
    MAINPAGE_PASTE_COUNT = 8
    MAX_CONTENT_LENGTH = 65536

    @webapp2.cached_property
    def session(self):
        s = self.session_store.get_session()
        return s

    def dispatch(self):
        # Set session storage
        self.session_store = sessions.get_store(request=self.request)

        # Set session ID
        sid = self.session.get('sid')
        if not sid:
            sid = uuid.uuid4().hex
            self.session['sid'] = sid

        if not self.session.get('auth_pass'):
            self.session['auth_pass'] = {}

        # Delete legacy info
        self.session.pop('view_grant', None)
        self.session.pop('foo', None)
        self.session.pop('bbb', None)

        appenv.JINJA_ENVIRONMENT.globals['auth_pass'] = self.session.get('auth_pass')
        appenv.JINJA_ENVIRONMENT.globals['auth_verify'] = Paste.verify
        appenv.JINJA_ENVIRONMENT.globals['session_id'] = sid
        appenv.JINJA_ENVIRONMENT.globals['host_url'] = self.request.host_url

        # Set the language
        appenv.set_language(self.request.cookies.get('lang'))

        # Set default header
        self.response.headers['Content-Type'] = 'text/html'

        # Start dispatching
        try:
            webapp2.RequestHandler.dispatch(self)
        finally:
            self.session_store.save_sessions(self.response)


    @webapp2.cached_property
    def session_id(self):
        return self.session.get('sid')
