#!/usr/bin/env bash

pybabel extract -F locale/babel.cfg -o locale/url_shortener.pot MainPage/html/

msgmerge -UN --backup=off locale/zh/LC_MESSAGES/url_shortener.po locale/url_shortener.pot
msgmerge -UN --backup=off locale/en/LC_MESSAGES/url_shortener.po locale/url_shortener.pot
msgmerge -UN --backup=off locale/tw/LC_MESSAGES/url_shortener.po locale/url_shortener.pot
