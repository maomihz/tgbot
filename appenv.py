import string
import random
import os
import gettext

import jinja2

import markdown
import bleach
from bleach_whitelist import markdown_tags, markdown_attrs


# Jinja environment
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(
        searchpath = os.path.join(os.path.dirname(__file__), 'MainPage', 'html')
    ),
    extensions=['jinja2.ext.i18n'],
    trim_blocks=True,
)

JINJA_ENVIRONMENT.globals['version'] = '0.2.4 20170209'


JINJA_ENVIRONMENT.filters['markdown'] = markdown.markdown
JINJA_ENVIRONMENT.filters['bleach'] = lambda md_html: bleach.linkify(
                                    bleach.clean(md_html, markdown_tags, markdown_attrs)
                                    )

# Security tokens of bots
bot_tokens = {
    'maomihz': '327663103:AAGyqEIF4Ua2l5cDf1dk1TuGW5sfrXkMtwM',
    'nfcompanion': '304538991:AAEEgnIPCsmZHpVyHVnV4O49tyLl19L6xPA',
    'maomihzpaste': '150326261:AAEuIGn_jFeN9ccej-xC3GlFxPnbY5s5yso'
}

# Return a random string
def randstr_from(length=8, fromstr=string.digits):
    chars = [random.choice(fromstr) for a in range(0,length)]
    return ''.join(chars)

def set_language(lang):
    if not lang in ['en', 'zh', 'tw']:
        lang = 'en'
    DIR='./locale'
    l = gettext.translation('url_shortener', localedir=DIR, languages=[lang], fallback=True)
    JINJA_ENVIRONMENT.install_gettext_translations(l, newstyle=False)
