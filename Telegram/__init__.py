from Chat import Chat
from Message import Message
from User import User
from PhotoSize import PhotoSize
from Update import Update
from Command import Command

from ReplyKeyboardMarkup import ReplyKeyboardMarkup
from KeyboardButton import KeyboardButton
from ReplyKeyboardRemove import ReplyKeyboardRemove

from google.appengine.api import urlfetch
import urllib
import json

class SendPhoto:
    def __init__(self, token, file_id=None, chat_id='@maomaobotbroadcast'):
        self.bot_token = token
        self.file_id = file_id
        self.chat_id = chat_id

    def set_file_id(self, file_id):
        self.file_id = file_id

    def send(self):
        if self.file_id:
            message_data = {'photo': self.file_id,
                    'chat_id': self.chat_id }
            # Set headers and url fetch
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            form_data = urllib.urlencode(message_data)
            result = urlfetch.fetch(
                url='https://api.telegram.org/bot' + self.bot_token + '/sendPhoto',
                payload=form_data,
                method=urlfetch.POST,
                headers=headers)
            return result.content

class SendSticker:
    def __init__(self, token, sticker=None, chat_id='@maomaobotbroadcast'):
        self.bot_token = token
        self.sticker = sticker
        self.chat_id = chat_id

    def set_sticker(self, sticker):
        self.sticker = sticker

    def send(self):
        if self.sticker:
            message_data = {'sticker': self.sticker,
                    'chat_id': self.chat_id }
            # Set headers and url fetch
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            form_data = urllib.urlencode(message_data)
            result = urlfetch.fetch(
                url='https://api.telegram.org/bot' + self.bot_token + '/sendSticker',
                payload=form_data,
                method=urlfetch.POST,
                headers=headers)
            return result.content

class SendMessage:
    def __init__(self, token, chat_id='@maomaobotbroadcast', text='',
            parse_mode=None,
            disable_web_page_preview=False,
            reply_to_message_id=None,
            reply_markup=None):
        self.bot_token = token
        self.chat_id = chat_id
        self.text = text
        self.parse_mode = parse_mode
        self.disable_web_page_preview = disable_web_page_preview
        self.reply_to_message_id = reply_to_message_id
        self.reply_markup = reply_markup

    def set_markdown(self):
        self.parse_mode = 'Markdown'

    def set_html(self):
        self.parse_mode = 'HTML'

    def set_disable_web_page_preview(self):
        disable_web_page_preview = True

    def set_text(self, text):
        self.text = text

    def set_chat_id(self, chat_id):
        self.chat_id = chat_id

    def set_reply_markup(self, reply_markup):
        self.reply_markup = reply_markup

    def remove_reply_markup(self):
        self.reply_markup = ReplyKeyboardRemove()

    def send(self):
        message_data = {'text': self.text,
                'chat_id': self.chat_id }
        if self.parse_mode in ['Markdown', 'HTML']:
            message_data['parse_mode'] = self.parse_mode
        if self.disable_web_page_preview:
            message_data['disable_web_page_preview'] = self.disable_web_page_preview
        if self.reply_to_message_id:
            message_data['reply_to_message_id'] = self.reply_to_message_id
        if self.reply_markup:
            message_data['reply_markup'] = self.reply_markup.to_json()
        # Set headers and url fetch
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        form_data = urllib.urlencode(message_data)
        if self.text:
            result = urlfetch.fetch(
                url='https://api.telegram.org/bot' + self.bot_token + '/sendMessage',
                payload=form_data,
                method=urlfetch.POST,
                headers=headers)
            return result.content

# Telegram class is used to use various telegram functions
class Telegram:
    def __init__(self, bot_token):
        self.bot_token = bot_token
