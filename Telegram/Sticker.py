from PhotoSize import PhotoSize

class Sticker:
    def __init__(self, sticker):
        self.file_id = sticker['file_id']
        self.width = sticker['width']
        self.height = sticker['height']
        self.thumb = PhotoSize.new(sticker['thumb'])
        self.emoji = sticker['emoji']
        self.file_size = sticker['file_size']

    @classmethod
    def new(cls, sticker):
        return None if not sticker else Sticker(sticker)

    def get_file_id(self):
        return self.file_id

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_thumb(self):
        return self.thumb

    def get_emoji(self):
        return self.emoji

    def get_file_size(self):
        return self.file_size
