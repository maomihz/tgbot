import json

from Message import Message

class Update:
    def __init__(self, update_json):
        update = json.loads(update_json)
        self.update_id = update.get('update_id')
        self.message = Message.new(update.get('message'))
        self.edited_message = Message.new(update.get('edited_message'))

    def get_update_id(self):
        return self.update_id

    def get_message(self):
        return self.message

    def get_edited_message(self):
        return self.edited_message
