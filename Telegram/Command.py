class Command:
    def __init__(self, text):
        split_command = filter(None, text.split())
        self.command = split_command[0]
        if self.command.startswith('/'):
            self.command = self.command[1:]

        self.command = self.command.split('@')[0]
        self.arguments = split_command[1:]

    def get_command(self):
        return self.command

    def get_arguments(self):
        return self.arguments
