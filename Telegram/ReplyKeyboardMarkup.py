from KeyboardButton import KeyboardButton

import json

class ReplyKeyboardMarkup:
    def __init__(self):
        self.keyboard = [[]]
        self.row_count = 0
        self.resize_keyboard = False
        self.one_time_keyboard = False

    def add_row(self):
        self.row_count = self.row_count + 1
        self.keyboard.append([])

    def add_key(self, text):
        self.keyboard[self.row_count].append(KeyboardButton(text))

    def set_one_time_keyboard(self):
        self.one_time_keyboard = True

    def set_resize_keyboard(self):
        self.resize_keyboard = True

    def get_keyboard(self):
        return self.keyboard

    def to_json(self):
        keyboard = [[j.__dict__ for j in i] for i in self.get_keyboard()]
        result = {'keyboard': keyboard}
        if self.one_time_keyboard:
            result['one_time_keyboard'] = True
        if self.resize_keyboard:
            result['resize_keyboard'] = True
        return json.dumps(result)
