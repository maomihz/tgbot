
class Chat:
    def __init__(self, chat):
        self.id = chat.get('id')
        self.type = chat.get('type')
        self.title = chat.get('title')
        self.username = chat.get('username')
        self.first_name = chat.get('first_name')
        self.last_name = chat.get('last_name')

    @classmethod
    def new(cls, chat):
        return None if not chat else Chat(chat)

    def get_id(self):
        return self.id

    def get_type(self):
        # Either private, group, supergroup or channel
        return self.type

    def get_title(self):
        return self.title

    def get_username(self):
        return self.username

    def get_first_name(self):
        return self.first_name

    def get_last_name(self):
        return self.last_name

    def is_private(self):
        return self.type == 'private'

    def is_group(self):
        return self.type == 'group'

    def is_supergroup(self):
        return self.type == 'supergroup'

    def is_channel(self):
        return self.type == 'channel'
