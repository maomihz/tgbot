import json

from User import User
from Chat import Chat
from PhotoSize import PhotoSize
from Sticker import Sticker

class Message:
    def __init__(self, message):
        self.message_id = message.get('message_id')
        self.from_user = User.new(message.get('from'))
        self.chat = Chat.new(message.get('chat'))
        self.forward_from = User.new(message.get('forward_from'))
        self.forward_from_chat = Chat.new(message.get('forward_from_chat'))
        self.reply_to_message = self.new(message.get('reply_to_message'))
        self.text = message.get('text')

        # Array of PhotoSize
        photo = message.get('photo')
        if photo:
            self.photo = [PhotoSize.new(p) for p in photo]
        else:
            self.photo = None

        self.sticker = Sticker.new(message.get('sticker'))

    @classmethod
    def new(cls, message):
        return None if not message else Message(message)

    def get_message_id(self):
        return self.message_id

    def get_from(self):
        return self.from_user

    def get_chat(self):
        return self.chat

    def get_forwarded_from(self):
        return self.forwarded_from

    def get_forwarded_from_chat(self):
        return self.forward_from_chat

    def get_text(self):
        return self.text

    def get_photo(self):
        return self.photo

    def get_sticker(self):
        return self.sticker
