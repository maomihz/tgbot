import json
class ReplyKeyboardRemove:
    def __init__(self):
        self.remove_keyboard = True

    def to_json(self):
        return json.dumps(self.__dict__)
