class User:
    def __init__(self, user):
        self.id = user.get('id')
        self.first_name = user.get('first_name')
        self.last_name = user.get('last_name')
        self.username = user.get('username')

    @classmethod
    def new(cls, user):
        return None if not user else User(user)

    def get_id(self):
        return self.id

    def get_first_name(self):
        return self.first_name

    def get_last_name(self):
        return self.last_name

    def get_user_name(self):
        return self.username
