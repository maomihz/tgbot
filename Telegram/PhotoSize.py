
class PhotoSize:
    def __init__(self, photosize):
        self.file_id = photosize.get('file_id')
        self.width = photosize.get('width')
        self.height = photosize.get('height')
        self.file_size = photosize.get('file_size')

    @classmethod
    def new(cls, photosize):
        return None if not photosize else PhotoSize(photosize)

    def get_file_id(self):
        return self.file_id

    def get_width(self):
        return self.width

    def get_height(self):
        return self.height

    def get_file_size(self):
        return self.file_size
